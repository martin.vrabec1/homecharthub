<?php

namespace App\Tests;

use App\Enums\Trend;
use App\Service\DataService;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TrendDataTest extends KernelTestCase
{

    private $stableTrendDiff = 0.5;

    private DataService $dataService;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $container = self::getContainer();
        $this->dataService = $container->get(DataService::class);
    }
    public function testZeroValues(): void
    {    
        $trend = $this->dataService->getTrendData(0,0);
        $this->assertSame(Trend::STABLE, $trend);

        $trend = $this->dataService->getTrendData(11.5,12);
        $this->assertSame(Trend::STABLE, $trend);
    }

    public function testRising(): void
    {    
        $trend = $this->dataService->getTrendData(26.7, 26);
        $this->assertSame(Trend::RISING, $trend);

        $trend = $this->dataService->getTrendData(-4, -5);
        $this->assertSame(Trend::RISING, $trend);
    }

    public function testFalling(): void
    {    
        $trend = $this->dataService->getTrendData(25.49, 26);
        $this->assertSame(Trend::FAILING, $trend);

        $trend = $this->dataService->getTrendData(-6, -3);
        $this->assertSame(Trend::FAILING, $trend);
    }

}
