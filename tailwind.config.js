/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./assets/**/*.js",
    "./templates/**/*.html.twig",
    "./templates/**/*.twig",
    "./public/assets/js/components/*.js",
  ],
  variants: {
    extend: {
      display: ["group-hover"]
    }
  },
  theme: {
    screens: {
      'desktop': '1280px',
    },
    extend: {
      colors: {
        'hd': {
          'dark-lue': '#183261',
          'dark-green': '#003C00',
          'darkest-green': '#002D00',
          'deep-teal': '#002F33',
          'deeper-teal': '#00282B',
          'light-silver': '#f2f2f2'
        }
      }
    },
  },
  plugins: [],
}