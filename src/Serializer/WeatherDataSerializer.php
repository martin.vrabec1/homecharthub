<?php

namespace App\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class WeatherDataSerializer {

    private $serializer;

    public function __construct()
    {
        $this->serializer =  new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
    }

    public function serializeToJson($object): string
    {
        return $this->serializer->serialize($object, 'json');
    }

    public function deserializeFromJson(string $json, string $className)
    {
        return $this->serializer->deserialize($json, $className, 'json');
    }


}