<?php 

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AssetController extends AbstractController {

    const MAX_AGE = 31536000;

    #[Route('routes.js', name:'routes.js')]
    public function routes()
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'text/javascript');

        $response->setCache(array(
            'max_age' => self::MAX_AGE,
            's_maxage' => self::MAX_AGE,
            'public' => true,
        ));

        return $this->render('assets/routes.html.twig', [], $response);

    }

}