<?php

namespace App\Controller;

use App\APIWeather\APIWeatherProvider;
use App\APIWeather\OpenWeatherMapProvider;
use App\Request\APIWeatherRequest\OpenWeatherMapRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController extends AbstractController {
    
    public static function getSubscribedServices(): array
{
    return array_merge(
        parent::getSubscribedServices(),
        [
            'app.api_weather_provider.open_weather_map_provider' => OpenWeatherMapProvider::class,
        ]
    );
}

}