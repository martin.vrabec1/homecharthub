<?php

namespace App\Controller\API;
use App\Repository\DeviceRepository;
use App\Repository\LocationRepository;
use App\Response\CurrentDataValueWithTrendResponse;
use App\Response\DewpointSensorCurrentValueDecorator;
use App\Response\SensorCurrentValueWithTrendResponse;
use App\Service\DataService;
use App\Service\ResponseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'api_')]
class PreviousSameTimeDataController extends AbstractController {

    private LocationRepository $lr;
    private DeviceRepository $dr;
    private DataService $ds;

    private ResponseService $rs;

    public function __construct(LocationRepository $lr, DeviceRepository $dr, DataService $ds, ResponseService $rs)
    {
        $this->lr = $lr;
        $this->dr = $dr;
        $this->ds = $ds;
        $this->rs = $rs;
    }

    #[Route('/previous', name:'previous_data', methods: ['POST'])]
    public function previousSameTimeLocationData(Request $request)
    {
        $id = $request->toArray()['id'];
        $location = $this->lr->find($id);

        if (!$location){
            throw new NotFoundHttpException();
        }

        $previousData = $this->rs->getPreviousDataResponse($location);

        return new JsonResponse($previousData);
    }

    #[Route('/previous/{id}', name:'previous_data_test', methods: ['GET'])]
    public function currentLocationDataTest($id)
    {
        $location = $this->lr->find($id);

        if (!$location){
            throw new NotFoundHttpException();
        }

        $devices = $this->dr->getAllDevicesByLocation($location);
        $domains = ['temperature', 'humidity'];

        $filteredDevices = array_filter($devices, function($item) use ($domains) {
            return in_array($item['domainClass'], $domains);
        });

        $previousData = $this->ds->getPreviousDaysData($filteredDevices);

        return new JsonResponse($previousData);
    }


}