<?php

namespace App\Controller\API;
use App\Repository\DeviceRepository;
use App\Repository\LocationRepository;
use App\Response\CurrentDataValueWithTrendResponse;
use App\Response\DewpointSensorCurrentValueDecorator;
use App\Response\SensorCurrentValueWithTrendResponse;
use App\Service\DataService;
use App\Service\ResponseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'api_')]
class CurrentDataController extends AbstractController {

    private LocationRepository $lr;
    private DeviceRepository $dr;
    private DataService $ds;
    private ResponseService $rs;

    public function __construct(LocationRepository $lr, DeviceRepository $dr, DataService $ds, ResponseService $rs)
    {
        $this->lr = $lr;
        $this->dr = $dr;
        $this->ds = $ds;
        $this->rs = $rs;
    }

    #[Route('/location', name:'current_data', methods: ['POST'])]
    public function currentLocationData(Request $request)
    {
        $id = $request->toArray()['id'];
        $location = $this->lr->find($id);

        if (!$location){
            throw new NotFoundHttpException();
        }
        
        $devices = $this->dr->getAllDevicesByLocation($location);

        $currentData = $this->ds->getCurrentData($devices);

        $toRet = $this->rs->getCurrentValuesResponse($currentData);
        
        return new JsonResponse($toRet->toArray());
    }

    #[Route('/location/{id}', name:'current_data_test', methods: ['GET'])]
    public function currentLocationDataTest($id)
    {
        $location = $this->lr->find($id);

        if (!$location){
            throw new NotFoundHttpException();
        }

        $devices = $this->dr->getAllDevicesByLocation($location);

        $currentDataResponses = [];
        $currentData = $this->ds->getCurrentData($devices);

        $domains = [];

        $toRet = new SensorCurrentValueWithTrendResponse();
        foreach ($currentData as $deviceData){
            $currentDataResponse = new CurrentDataValueWithTrendResponse();
            $avg = $this->ds->getMean($deviceData['field'], $deviceData['entityId'], $deviceData['unit']);
            $trend = $this->ds->getTrendData($deviceData['raw'], $avg);

            $currentDataResponse
                ->setDevice($deviceData['device'])
                ->setTime($deviceData['time'])
                ->setValue($deviceData['value'])
                ->setTrend($trend)
                ->setDomain($deviceData['domainClass'])
                ->setRawValue($deviceData['raw'])
                ->setUnit($deviceData['unit'])
                ->setName($deviceData['name'])
            ;

            $domains[] = $currentDataResponse->getDomain();

            $toRet->addEntityResponse($currentDataResponse);
        }
 
        //if sensor contains temperatura and humidity, calculate also dewpoint
        if (
            in_array('temperature', $domains) &&
            in_array('humidity', $domains)
        ) {
            $toRet = new DewpointSensorCurrentValueDecorator($toRet);
        }

        return new JsonResponse($toRet->toArray());
    }


}