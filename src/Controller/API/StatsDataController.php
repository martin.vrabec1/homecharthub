<?php

namespace App\Controller\API;
use App\Repository\DeviceRepository;
use App\Repository\LocationRepository;
use App\Service\DataService;
use App\Service\ResponseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'api_')]
class StatsDataController extends AbstractController {

    private LocationRepository $lr;
    private DeviceRepository $dr;
    private DataService $ds;

    private ResponseService $rs;

    public function __construct(LocationRepository $lr, DeviceRepository $dr, DataService $ds, ResponseService $rs)
    {
        $this->lr = $lr;
        $this->dr = $dr;
        $this->ds = $ds;
        $this->rs = $rs;
    }

    #[Route('/stats', name:'stats_data', methods: ['POST'])]
    public function statsData(Request $request)
    {
        $id = $request->toArray()['id'];
        $location = $this->lr->find($id);

        if (!$location){
            throw new NotFoundHttpException();
        }

        $statsData = $this->rs->getStatsResponse($location);

        return new JsonResponse($statsData->toArray());
    }

    #[Route('/stats/{id}', name:'stats_data_test', methods: ['GET'])]
    public function statsDataTest($id)
    {
        $location = $this->lr->find($id);

        if (!$location){
            throw new NotFoundHttpException();
        }

        $statsData = $this->rs->getStatsResponse($location);

        return new JsonResponse($statsData);
    }


}