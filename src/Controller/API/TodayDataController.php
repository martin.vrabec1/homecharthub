<?php

namespace App\Controller\API;
use App\APIWeather\APIWeatherProcessor;
use App\Controller\BaseController;
use App\Repository\DeviceRepository;
use App\Repository\LocationRepository;
use App\Service\APIService;
use App\Service\ChartService;
use App\Service\DataService;
use App\Service\ResponseService;
use App\Service\WeatherService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'api_')]
class TodayDataController extends BaseController {

    private LocationRepository $lr;
    private DeviceRepository $dr;
    private DataService $ds;
    private APIService $apiService;
    private ResponseService $rs;
    private WeatherService $ws;
    private ChartService $cs;


    public function __construct(LocationRepository $lr, DeviceRepository $dr, DataService $ds, ResponseService $rs, APIService $apiService, WeatherService $ws, ChartService $cs)
    {
        $this->lr = $lr;
        $this->dr = $dr;
        $this->ds = $ds;
        $this->rs = $rs;
        $this->apiService = $apiService;
        $this->ws = $ws;
        $this->cs = $cs;
    }

    #[Route('/today', name:'today_data', methods: ['POST'])]
    public function todayData(Request $request)
    {
        $id = $request->toArray()['id'];
        $location = $this->lr->find($id);

        if (!$location){
            throw new NotFoundHttpException();
        }

        $toRet = $this->rs->getTodayMinMaxDataResponse($location);


        return new JsonResponse($toRet->toArray());
    }

    #[Route('/today_chart_data', name:'today_chart_data', methods: ['POST'])]
    public function todayChartData(Request $request)
    {
        $id = $request->toArray()['id'];
        $location = $this->lr->find($id);

        if (!$location){
            throw new NotFoundHttpException();
        }

        $chartData = $this->rs->getOneDayChart($location);
        $chart = $this->cs->buildChart($chartData);

        // $toRet = json_encode($chart);

        $toRet = [
            'data' => json_encode($chart->getData()),
            'options' => json_encode($chart->getOptions())
        ];

        return new JsonResponse($toRet);
    }

    #[Route('/today/{id}', name:'today_data_test', methods: ['GET'])]
    public function todayDataTest($id, APIService $apiService)
    {
        $location = $this->lr->find($id);

        if (!$location){
            throw new NotFoundHttpException();
        }
        
        $toRet = $this->rs->getTodayMinMaxDataResponse($location);

        return new JsonResponse($toRet->toArray());
    }


}