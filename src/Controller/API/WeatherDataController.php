<?php

namespace App\Controller\API;
use App\APIWeather\APIWeatherProcessor;
use App\Controller\BaseController;
use App\Repository\DeviceRepository;
use App\Repository\LocationRepository;
use App\Service\APIService;
use App\Service\DataService;
use App\Service\ResponseService;
use App\Service\WeatherService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'api_')]
class WeatherDataController extends BaseController {

    private LocationRepository $lr;
    private DeviceRepository $dr;
    private DataService $ds;
    private APIService $apiService;
    private ResponseService $rs;
    private WeatherService $ws;


    public function __construct(LocationRepository $lr, DeviceRepository $dr, DataService $ds, ResponseService $rs, APIService $apiService, WeatherService $ws)
    {
        $this->lr = $lr;
        $this->dr = $dr;
        $this->ds = $ds;
        $this->rs = $rs;
        $this->apiService = $apiService;
        $this->ws = $ws;
    }

    #[Route('/weather', name:'weather_data', methods: ['POST'])]
    public function weatherData(Request $request)
    {
        $id = $request->toArray()['id'];
        $location = $this->lr->find($id);

        if (!$location){
            throw new NotFoundHttpException();
        }

        $toRet = $this->rs->getWeatherResponse($location, $this->container);

        return new JsonResponse($toRet->toArray());
    }

    #[Route('/weather/{id}', name:'weather_data_test', methods: ['GET'])]
    public function currentWeatherDataTest($id, APIService $apiService)
    {
        $location = $this->lr->find($id);

        if (!$location){
            throw new NotFoundHttpException();
        }
        
        $toRet = $this->rs->getWeatherResponse($location, $this->container);

        return new JsonResponse($toRet->toArray());
    }


}