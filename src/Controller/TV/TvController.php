<?php

namespace App\Controller\TV;
use App\Controller\BaseController;
use App\Repository\DeviceRepository;
use App\Repository\LocationRepository;
use App\Service\APIService;
use App\Service\ChartService;
use App\Service\DataService;
use App\Service\ResponseService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/tv', name: 'tv_')]

class TvController extends BaseController {

    private LocationRepository $locationRepository;

    private APIService $apiService;

    private DeviceRepository $dr;

    private DataService $ds;

    private ResponseService $rs;

    public function __construct(LocationRepository $locationRepository, APIService $apiService, DeviceRepository $dr, DataService $ds, ResponseService $rs)
    {
        $this->locationRepository = $locationRepository;
        $this->apiService = $apiService;
        $this->dr = $dr;
        $this->ds = $ds;
        $this->rs = $rs;
    }

    #[Route('/', name: 'dashboard')]
    public function tvDashboard(Request $request, ChartService $chartService): Response
    {
        $id = 1;
        $location = $this->locationRepository->find($id);

        $locations = $this->locationRepository->findAll();

        $devices = $this->dr->getAllDevicesByLocation($location);

        $currentData = $this->ds->getCurrentData($devices);

        $currentValues = $this->rs->getCurrentValuesResponse($currentData);

        $locationsArray = array_map(function($item){
            return $item->toArray();
        }, $locations);

        $remainingLocations = array_filter($locationsArray, function($item) use ($location){
            return $item['id'] !== $location->getId();
        });

        $previousData = $this->rs->getPreviousDataResponse($location);

        $statsData = $this->rs->getStatsResponse($location);
        $weatherData = $this->rs->getWeatherResponse($location, $this->container);
    
        $todayMinMaxData = $this->rs->getTodayMinMaxDataResponse($location);

        $oneDayChartData = $this->rs->getOneDayChart($location);

        $chart = $chartService->buildChart($oneDayChartData);

        return $this->render('dashboard/tv.html.twig',[
            'locations' => $locationsArray,
            'remaining_locations' => $remainingLocations,
            'location' => $location,
            'currentValues' => $currentValues->toArray(),
            'previousData' => $previousData,
            'statsData' => $statsData,
            'weatherData' => $weatherData->toArray(),
            'todayMinMaxData' => $todayMinMaxData,
            'chart' => $chart
        ]);
    }


}