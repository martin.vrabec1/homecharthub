<?php

namespace App\Controller;

use App\Controller\TV\TvController;
use App\Repository\LocationRepository;
use App\Service\InfluxService;
use App\Service\TvAppService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    private LocationRepository $locationRepository;

    private InfluxService $influxService;

    private TvAppService $tvAppService;

    public function __construct(LocationRepository $locationRepository, InfluxService $influxService, TvAppService $tvAppService)
    {
        $this->locationRepository = $locationRepository;
        $this->influxService = $influxService;
        $this->tvAppService = $tvAppService;
    }

    #[Route('/', name: 'app_dashboard')]
    public function index(): Response
    {

        if ($this->tvAppService->isTvMode()){
            return $this->redirectToRoute('tv_dashboard');
        }

        $locations = $this->locationRepository->findAll();

        return $this->render('dashboard/index.html.twig', [
            'locations' => array_map(function($item){
                return $item->toArray();
            }, $locations),
        ]);
    }
}
