<?php

namespace App\Controller\Admin;

use App\Entity\Device;
use App\Entity\Entity;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;

class DeviceCrudController extends AbstractCrudController
{

    private \App\Repository\EntityRepository $entityRepository;

    public function __construct(\App\Repository\EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public static function getEntityFqcn(): string
    {
        return Device::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            AssociationField::new('entities'),
        ];
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $collections = $entityInstance->getEntities();

        foreach ($collections as $collection){
            $collection->setDevice($entityInstance);
        }
    
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->removeDevices($entityInstance);
        $this->persistEntity($entityManager, $entityInstance);
    }

    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->removeDevices($entityInstance);

        $entityManager->remove($entityInstance);
        $entityManager->flush();
    }

    private function removeDevices($entity) {
        $entities = $this->entityRepository->findAllByDevice($entity);
        $this->entityRepository->removeDeviceFromEntities($entities);
    }


}
