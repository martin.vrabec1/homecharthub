<?php

namespace App\Controller\Admin;

use App\Enums\UserRoles;
use App\Entity\Domain;
use App\Entity\Entity;
use App\Entity\Device;
use App\Entity\Location;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        if (!$this->getUser() && !$this->isGranted(UserRoles::ADMIN)) {
            return $this->redirect('/login');
        }

        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(DomainCrudController::class)->generateUrl());


        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Html');
    }

    public function configureMenuItems(): iterable
    {
        return [
            // MenuItem::linkToDashboard('Admin', 'fa fa-home'),
            MenuItem::linkToCrud('Domény', 'fas fa-list', Domain::class),
            MenuItem::linkToCrud('Entity', 'fas fa-list', Entity::class),
            MenuItem::linkToCrud('Zariadenia', 'fas fa-list', Device::class),
            MenuItem::linkToCrud('Lokality', 'fas fa-list', Location::class),
        ];
    }
}
