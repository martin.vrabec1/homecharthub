<?php

namespace App\Controller\Admin;

use App\Entity\Location;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use Doctrine\ORM\EntityManagerInterface;


class LocationCrudController extends AbstractCrudController
{

    private \App\Repository\DeviceRepository $deviceRepository;

    public function __construct(\App\Repository\DeviceRepository $deviceRepository)
    {
        $this->deviceRepository = $deviceRepository;
    }

    public static function getEntityFqcn(): string
    {
        return Location::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            NumberField::new('lat', 'Latitude'),
            NumberField::new('lng', 'Longitude'),
            AssociationField::new('devices'),
        ];
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $collections = $entityInstance->getDevices();

        foreach ($collections as $collection){
            $collection->setLocation($entityInstance);
        }
    
        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->removeLocations($entityInstance);
        $this->persistEntity($entityManager, $entityInstance);
    }

    public function deleteEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->removeLocations($entityInstance);

        $entityManager->remove($entityInstance);
        $entityManager->flush();
    }

    private function removeLocations($device) {
        $devices = $this->deviceRepository->findAllByLocation($device);
        $this->deviceRepository->removeLocationFromDevice($devices);
    }

}
