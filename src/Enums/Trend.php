<?php 

namespace App\Enums;

class Trend {
    const RISING = "rising";
    const FAILING = "failing";
    const STABLE = "stable";
}

?>