<?php

namespace App\Enums;

enum UserRoles:string {
    case ADMIN = 'ROLE_ADMIN';
}