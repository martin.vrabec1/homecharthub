<?php

namespace App\Service;
use App\Entity\Location;
use App\Entity\Weather;
use App\Repository\WeatherRepository;

class WeatherService {

    private WeatherRepository $weatherRepository;

    public function __construct(WeatherRepository $weatherRepository)
    {
        $this->weatherRepository = $weatherRepository;
    }

    public function saveWeatherData(string $dataResponse, Location $location, string $provider)
    {
        $datetime = new \DateTime;

        $this->weatherRepository->insertWeatherData($datetime, $dataResponse, $location, $provider);
    }

    public function getWeatherData(Location $location):?Weather
    {
        return $this->weatherRepository->getWeatherData($location);
    }

}