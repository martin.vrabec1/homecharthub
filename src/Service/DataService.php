<?php

namespace App\Service;
use App\Entity\Device;
use App\Entity\Location;
use App\Enums\DataOperations;
use App\Enums\Trend;

class DataService {

    private InfluxService $is;
    private float $stableTrendDiff;

    private $allowedDomains;

    public function __construct(InfluxService $is, $stableTrendDiff, $allowedDomains)
    {
        $this->is = $is;
        $this->stableTrendDiff = $stableTrendDiff;
        $this->allowedDomains = explode(',', $allowedDomains);
    }

    public function getCurrentData($data)
    {

        foreach ($data as &$item){
            $qb = $this->is->getQueryBuilder()
                ->select(sprintf("last(%s) AS raw", $item['field']))
                ->from($item['unit'])
                ->where([sprintf("entity_id='%s'", $item['entityId'])])
            ;

            $points = $qb->getResultSet()->getPoints();
            $influxValues = reset($points);

            $item = $item + $influxValues;

            if (isset($item['time'])){
                $item['rawTime'] = $item['time'];
                $item['time'] = Timestamp::format($item['time']);
            }

            if (isset($item['raw']) && isset($item['unit'])){
                $item['value'] = sprintf("%s %s", $item['raw'], $item['unit']);
            }
        }

        
        return $data;
    }

    /**
     * Get mean value according to field, entity, unit and limit for inlux db
     *
     * @param [type] $field
     * @param [type] $entity
     * @param [type] unit
     * @return void
     */
    public function getMean($field, $entity, $unit): float
    {

        //select values older than 1 hour

        $qb = $this->is->getQueryBuilder()
            ->mean($field)
            ->from($unit)
            ->where([sprintf("entity_id='%s'", $entity), "time > now()-1h"])
        ;

        $points = $qb->getResultSet()->getPoints();
       
        $avg = 0;
        if (is_array($points) && count($points) > 0){
            $avg = reset($points)['mean'];
        }

        return $avg;
    }

    public function getTrendData($raw, $avg):string|null
    {

        if (!is_numeric($raw)){
            return null;
        }

        $diff = $raw - $avg;
    
        if (abs($diff) <= $this->stableTrendDiff) {
            return Trend::STABLE;
        }
        
        if ($diff > 0) {
            return Trend::RISING;
        } else {
            return Trend::FAILING;
        }
    }

    public function getPreviousDaysData(array $domains)
    {
        $ret = [];
        foreach ($domains as $domain) {
            $domainClass = $domain['domainClass'];
            
            $dataYesterday = $this->fetchPreviousData($domain, $domain['entityId'], $domain['field'], 1, 0);
            $dataTwoDaysAgo = $this->fetchPreviousData($domain, $domain['entityId'], $domain['field'], 2, 1);
            $dataThreeDaysAgo = $this->fetchPreviousData($domain, $domain['entityId'], $domain['field'], 3, 2);
            $dataYearBefore = $this->fetchPreviousData($domain, $domain['entityId'], $domain['field'], 365, 364);
            
            isset ($dataYesterday) && $ret[$dataYesterday['date']][$domainClass] = $dataYesterday['value'];
            isset ($dataTwoDaysAgo) && $ret[$dataTwoDaysAgo['date']][$domainClass] = $dataTwoDaysAgo['value'] ?? null;
            isset ($dataThreeDaysAgo) && $ret[$dataThreeDaysAgo['date']][$domainClass] = $dataThreeDaysAgo['value'] ?? null;
            isset ($dataYearBefore) && $ret[$dataYearBefore['date']][$domainClass] = $dataYearBefore['value'] ?? null;
        }

        return $ret;
    }

    public function getFirstValue($field, $entity, $unit)
    {
        $qb = $this->is->getQueryBuilder()
            ->select($field)
            ->from($unit)
            ->where([sprintf("entity_id='%s'", $entity)])
            ->orderBy("time","ASC")
            ->limit(1)
        ;
        
        return $qb->getResultSet()->getPoints();
    }

    public function getCount($field, $entity, $unit)
    {
        $qb = $this->is->getQueryBuilder()
            ->select(sprintf("COUNT(%s)",$field))
            ->from($unit)
            ->where([sprintf("entity_id='%s'", $entity)])
        ;
        
        return $qb->getResultSet()->getPoints();
    }

    public function getMinMax($field, $entity, $unit, $operation = DataOperations::MAX, $from = null, $to = null)
    {
        $qb = $this->is->getQueryBuilder();

        switch ($operation) {
            case DataOperations::MAX: {
                $qb->select(sprintf("MAX(%s)", $field));
                break;
            }
            case DataOperations::MIN: {
                $qb->select(sprintf("MIN(%s)", $field));
                break;
            }
            default: 
                throw new \InvalidArgumentException('Invalid operation specified.');
        }

        InfluxDateRangeFilter::filterQueryBuilder($qb, $from, $to);
        
        $qb
            ->from($unit)
            ->where([sprintf("entity_id='%s'", $entity)]);

        
        return $qb->getResultSet()->getPoints();
    }

    public function getDateRangeData($field, $entity, $unit, $from, $to): array
    {
        $qb = $this->is->getQueryBuilder();
        
        $qb
            ->mean($field)
            ->from($unit)
            ->where([sprintf("entity_id='%s'", $entity)]);

        InfluxDateRangeFilter::filterQueryBuilder($qb, $from, $to);

        $qb->groupBy('time(10m) FILL(previous)');
        
        return $qb->getResultSet()->getPoints();
    }

    public static function calculateDewPoint($temperature, $humidity) {
        return 
            pow($humidity / 100, 0.125) * (112 + 0.9 * $temperature) + 0.1 * $temperature -112;
    }

    private function fetchPreviousData($domain, $entityId, $field, $fromDaysAgo, $toDaysAgo) {
        $builder = $this->is->getQueryBuilder()
            ->select($field)
            ->from($domain['unit'])
            ->where([
                sprintf("entity_id='%s'", $entityId),
                "time >= now()-" . $fromDaysAgo . "d",
                "time <= now()-" . $toDaysAgo . "d"
            ])
            ->limit(1);
    
        $results = $builder->getResultSet()->getPoints();
    
        if (is_array($results) && reset($results)) {
            $result = reset($results);
            $date = Timestamp::formatDay($result['time']);
            return ['date' => $date, 'value' => sprintf("%s %s", $result['value'], $domain['unit'] )];
        } else {
            return null;
        }
    }

    public function getDataAllowedDomains($domains) {

        $allowedDomains = $this->allowedDomains;
        
        $toRet = array_filter($domains, function($item) use ($allowedDomains) {
            return in_array($item['domainClass'], $allowedDomains);
        });

        return $toRet;
    }
}