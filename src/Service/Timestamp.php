<?php

namespace App\Service;
use DateTimeImmutable;

class Timestamp {

    const DIRECTION_DOWN = 'down';
    const DIRECTION_UP = 'up';

    public static function round($timestamp, $direction)
    {
        if ($timestamp instanceof \DateTime) {
            $timestamp = $timestamp->getTimestamp() - (date('Z', $timestamp->getTimestamp()) * 1);
        }
        if (!preg_match('/^[0-9]+$/', $timestamp)) {
            $timestamp = strtotime($timestamp);
        }
        $date = date('Y-m-d', $timestamp);
        if ($direction == static::DIRECTION_DOWN) {
            return strtotime($date . ' 00:00:00');
        }
        if ($direction == static::DIRECTION_UP) {
            return strtotime($date . ' 23:59:00');
        }
        throw new \InvalidArgumentException('invalid direction');
    }

    public static function roundDown($timestamp)
    {
        return static::round($timestamp, self::DIRECTION_DOWN);
    }

    public static function roundUp($timestamp)
    {
        return static::round($timestamp, self::DIRECTION_UP);
    }

    public static function getTime($timestamp){
        $date = date('H:i', $timestamp);
    
        return $date;
    }

    public static function format($timestamp) 
    {
        if ($timestamp instanceof \DateTime){
            $date = $timestamp;
        } else {
            $date = new \DateTime($timestamp);
            $date->setTimezone(new \DateTimeZone('Europe/Bratislava'));
        }
        return $date->format('d. n. Y H:i');
    }

    public static function formatDay($timestamp) 
    {
        $date = new \DateTime($timestamp);
        $date->setTimezone(new \DateTimeZone('Europe/Bratislava'));

        return $date->format('j. n. Y');
    }

}

