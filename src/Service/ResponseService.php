<?php

namespace App\Service;
use App\APIWeather\APIWeatherProcessor;
use App\APIWeather\APIWeatherProviderInterface;
use App\Entity\Location;
use App\Entity\Weather;
use App\Enums\DataOperations;
use App\Repository\DeviceRepository;
use App\Repository\LocationRepository;
use App\Response\CurrentDataValueWithTrendResponse;
use App\Response\DataForChatItemResponse;
use App\Response\DataForChatResponse;
use App\Response\DateTimeValueResponse;
use App\Response\DewpointSensorCurrentValueDecorator;
use App\Response\MinMaxDateRangeDomainResponse;
use App\Response\MinMaxDateRangeResponse;
use App\Response\SensorCurrentValueWithTrendResponse;
use App\Response\StatsDataResponse;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ResponseService {

    private LocationRepository $lr;
    private DeviceRepository $dr;
    private DataService $ds;
    private WeatherService $ws;

    private ChartService $cs;

    public function __construct(LocationRepository $lr, DeviceRepository $dr, DataService $ds, WeatherService $ws, ChartService $cs)
    {
        $this->lr = $lr;
        $this->dr = $dr;
        $this->ds = $ds;
        $this->ws = $ws;
        $this->cs = $cs;
    }

    public function getCurrentValuesResponse($data)
    {
        $toRet = new SensorCurrentValueWithTrendResponse();

        $domains = [];
        
        foreach ($data as &$deviceData){
            $currentDataResponse = new CurrentDataValueWithTrendResponse();
            $avg = $this->ds->getMean($deviceData['field'], $deviceData['entityId'], $deviceData['unit']);
            $trend = $this->ds->getTrendData($deviceData['raw'], $avg);

            $currentDataResponse
                ->setDevice($deviceData['device'])
                ->setTime($deviceData['time'])
                ->setValue($deviceData['value'])
                ->setTrend($trend)
                ->setDomain($deviceData['domainClass'])
                ->setRawValue($deviceData['raw'])
                ->setUnit($deviceData['unit'])
                ->setName($deviceData['name'])
            ;

            $domains[] = $currentDataResponse->getDomain();

            $toRet->addEntityResponse($currentDataResponse);
        }

        //if sensor contains temperatura and humidity, calculate also dewpoint
        if (
            in_array('temperature', $domains) &&
            in_array('humidity', $domains)
        ) {
            $toRet = new DewpointSensorCurrentValueDecorator($toRet);
        }

        return $toRet;
    }

    public function getPreviousDataResponse(Location $location)
    {
        $domains = ['temperature', 'humidity'];
        $devices = $this->dr->getAllDevicesByLocation($location);

        $filteredDevices = array_filter($devices, function($item) use ($domains) {
            return in_array($item['domainClass'], $domains);
        });

        $previousData = $this->ds->getPreviousDaysData($filteredDevices);
   
        return $previousData;
    }


    public function getStatsResponse(Location $location)
    {
        $domain = "temperature";

        $devices = $this->dr->getAllDevicesByLocation($location);

        $temperatureDevice = array_filter($devices, function($item) use ($domain) {
            return $item['domainClass'] == $domain;
        });

        $device = reset($temperatureDevice);

        //firstValueResult
        $firstValueResult = $this->ds->getFirstValue($device['field'], $device['entityId'], $device['unit']);
        $firstValue = reset($firstValueResult);
        $firstValueTime = Timestamp::format($firstValue['time']);

        //countResult
        $countValueResult = $this->ds->getCount($device['field'], $device['entityId'], $device['unit']);
        $countValue = reset($countValueResult)['count'];

        $minValueResult = $this->ds->getMinMax($device['field'], $device['entityId'], $device['unit'], DataOperations::MIN);
        $maxValueResult = $this->ds->getMinMax($device['field'], $device['entityId'], $device['unit'], DataOperations::MAX);

        $minValueResult = reset($minValueResult);
        $maxValueResult = reset($maxValueResult);

        $min = new DateTimeValueResponse();
        $min
            ->setDatetime(Timestamp::format($minValueResult['time']))
            ->setValue(sprintf("%s %s", $minValueResult['min'], $device['unit']));
        ;
        $max = new DateTimeValueResponse();
        $max
            ->setDatetime(Timestamp::format($maxValueResult['time']))
            ->setValue(sprintf("%s %s", $maxValueResult['max'], $device['unit']));
        ;

        $response = new StatsDataResponse();
        $response
            ->setCount($countValue)
            ->setFirstValueTime($firstValueTime)
            ->setMax($max)
            ->setMin($min)
        ;

        return $response;
    }

    public function getWeatherResponse(Location $location, ContainerInterface $container)
    {
        
        $weather = $this->ws->getWeatherData($location);

        if (!$weather) {
            throw new NotFoundHttpException();
        }

        $weatherProvider = $container->get($weather->getProvider());

        $weatherProcessor = new APIWeatherProcessor();
        $weatherProcessor->setAPIWeatherProvider($weatherProvider);

        $toRet = $weatherProcessor->processResponse($weather);

        return $toRet;
    }

    public function getTodayMinMaxDataResponse(Location $location)
    {

        $devices = $this->dr->getAllDevicesByLocation($location);

        $filterEntitiesByDomain = $this->ds->getDataAllowedDomains($devices);

        $from = new \DateTime('today midnight');
        $to = new \DateTime('today 23:59:59');

        $toRet = new MinMaxDateRangeResponse();

        foreach ($filterEntitiesByDomain as $entity){
            $domainResponse = new MinMaxDateRangeDomainResponse();
            $max = $this->ds->getMinMax($entity['field'], $entity['entityId'], $entity['unit'], DataOperations::MAX, $from, $to);
            $min = $this->ds->getMinMax($entity['field'], $entity['entityId'], $entity['unit'], DataOperations::MIN, $from, $to);
            $domainResponse->setDomainClass($entity['domainClass']);
            $domainResponse->setDomainName($entity['domainName']);
            $domainResponse->setMax(sprintf("%s %s", reset($max)['max'], $entity['unit']));
            $domainResponse->setMin(sprintf("%s %s", reset($min)['min'], $entity['unit']));

            $toRet->addDomainResponse($domainResponse);
        }

        return $toRet;
    }

    public function getOneDayChart(Location $location): DataForChatResponse
    {
        $devices = $this->dr->getAllDevicesByLocation($location);

        $entities = $this->ds->getDataAllowedDomains($devices);

        $to = new \DateTime('now');
        $from = new \DateTime('-1 day');

        $chatResponse = new DataForChatResponse(); 


        $index = 0;
        foreach ($entities as $entity) {
            $chatItem = new DataForChatItemResponse();
            $dateRangeData = $this->ds->getDateRangeData($entity['field'], $entity['entityId'], $entity['unit'], $from, $to);
            $map = array_map(function($item) use($index) {
                $time = $item['time'];
                $time = new \DateTime($time);
                $time = $time->format('j.n.Y H:i');
                return [
                    'x' => $time,
                    'y' => $item['mean'],
                ];
            }, $dateRangeData);
            $chatItem->setData($map);
            $chatItem->setColor($entity['color']);
            $chatItem->setLabel($entity['domainName']);
            $chatItem->setYAxisId('y-'.$entity['domainName']);

            $chatResponse->addChatItemResponse($chatItem);
            $index++;
        }

        return $chatResponse;
    }

}