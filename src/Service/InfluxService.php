<?php

namespace App\Service;
use InfluxDB\ResultSet;

class InfluxService {

    private \InfluxDB\Database $db;

    public function __construct(string $influxUrl)
    {
        $this->db = \InfluxDB\Client::fromDSN($influxUrl);
    }

    public function getQueryBuilder(): \InfluxDB\Query\Builder
    {
        return $this->db->getQueryBuilder();
    }

    public function query(string $string): ResultSet
    {
        return $this->db->query($string);
    }

}