<?php

namespace App\Service;
use App\Entity\Employer;
use App\Enums\UserRoles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserService {

    private UserPasswordHasherInterface $encoder;

    private EntityManagerInterface $em;

    public function __construct(
        UserPasswordHasherInterface $encoder,
        EntityManagerInterface $em
    ) {
        $this->encoder = $encoder;
        $this->em = $em;
    }

    public function createAdmin($env):void
    {
        $admin = new Employer();
        $admin
            ->setRoles([UserRoles::ADMIN])
            ->setUsername('root')
            ->setPassword(
                $this->encoder->hashPassword(
                    $admin,
                    $env == 'prod' ? 'veHu3pNN994uv04' : 'root'
                )
            );

        $this->em->persist($admin);
        $this->em->flush();
    }
}