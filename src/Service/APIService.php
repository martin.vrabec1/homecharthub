<?php

namespace App\Service;
use GuzzleHttp\Client;

class APIService {

    private $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client([
            'debug' => true
        ]);
    }

    public function get($url, $queryParams = [])
    {
        $response = $this->httpClient->request('GET', $url, ['query' => $queryParams]);
    
        return $response->getBody()->getContents();
    }

    public function post($url, $postData = [])
    {
        $response = $this->httpClient->request('POST', $url, ['form_params' => $postData]);

        return $response->getBody()->getContents();
    }

}