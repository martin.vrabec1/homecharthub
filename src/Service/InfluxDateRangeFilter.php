<?php

namespace App\Service;

class InfluxDateRangeFilter {

    public static function filterQueryBuilder($qb, $dateFrom = null, $dateTo = null) 
    {

        if ($dateFrom != null){
            $qb->where([sprintf("time >= '%s'", $dateFrom->format(\DateTime::ATOM))]);
        }

        if ($dateTo != null){
            $qb->where([sprintf("time < '%s'", $dateTo->format(\DateTime::ATOM))]);
        }

        return $qb;
    }
}