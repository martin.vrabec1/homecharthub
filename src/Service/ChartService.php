<?php

namespace App\Service;

use App\Response\DataForChatResponse;
use Symfony\UX\Chartjs\Builder\ChartBuilder;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class ChartService
{

    private $chartBuilder;

    public function __construct()
    {
        $this->chartBuilder = new ChartBuilder;
    }

    public function buildChart(DataForChatResponse $data): Chart
    {
        $dataItems = $data->getChatItems();

        $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);

        $datasets = [];

        foreach ($dataItems as $item) {
            $datasets[] = [
                'label'           => $item->getLabel(),
                'data'            => $item->getData(),
                'backgroundColor' => $item->getColor(),
                'borderColor'     => $item->getColor(),
                'yAxisID'         => $item->getYAxisId(),
                'borderWidth'     => 2
            ];
        }

        $chart->setData([
            'datasets' => $datasets
        ]);


        $chart->setOptions([
            'elements'    => [
                'point' => [
                    'radius' => 0
                ]
            ],
            'interaction' => [
                'mode'      => 'index',
                'intersect' => false,
            ],
            'scales'=> [
                'y-Teplota'=> [
                    'type'     => 'linear',
                    'position' => 'left',
                    'stacked'  => false,
                ],
                'y-Vlhkosť'=> [
                    'type'     => 'linear',
                    'position' => 'left',
                    'stacked'  => false,
                    'max'   => 100,
                    'min'   => 0                
                ],
                'y-Svetelnosť'=> [
                    'type'     => 'linear',
                    'position' => 'right',
                    'stacked'  => false,
                    'min'   => 0                
                ],
                'y-Tlak'=> [
                    'type'     => 'linear',
                    'position' => 'right',
                    'stacked'  => false,
                ]
            ]
        ]);

        return $chart;
    }

}