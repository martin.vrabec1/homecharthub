<?php

namespace App\Service;
use Symfony\Component\HttpFoundation\RequestStack;

class TvAppService {

    private RequestStack $request;
    const TV_AGENTS = ['SMART-TV'];

    public function __construct(RequestStack $request)
    {
        $this->request = $request;
        if (!$this->request->getCurrentRequest()) {
            return;
        }
    }

    public function isTvMode()
    {
        $userAgent = $this->request->getCurrentRequest();
        
        foreach (self::TV_AGENTS as $agent) {
            if (stripos($userAgent, $agent)) {
                return true;
            }
        }
    }

}