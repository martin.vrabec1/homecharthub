<?php

namespace App\Repository;

use App\Entity\Location;
use App\Entity\Weather;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Weather>
 *
 * @method Weather|null find($id, $lockMode = null, $lockVersion = null)
 * @method Weather|null findOneBy(array $criteria, array $orderBy = null)
 * @method Weather[]    findAll()
 * @method Weather[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Weather::class);
    }

    public function insertWeatherData(\DateTimeInterface $datetime, string $dataResponse, Location $location, string $provider)
    {
        $weather = new Weather();
        $weather
            ->setDatetime($datetime)
            ->setResponse($dataResponse)
            ->setProvider($provider)
            ->setLocation($location)
        ;

        $this->_em->persist($weather);
        $this->_em->flush();
    }

    public function getWeatherData(Location $location)
    {
        return $this->createQueryBuilder('l')
            ->where('l.location=:location')
            ->orderBy('l.datetime','DESC')
            ->setMaxResults(1)
            ->setParameter('location', $location)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

//    /**
//     * @return Weather[] Returns an array of Weather objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('w')
//            ->andWhere('w.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('w.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Weather
//    {
//        return $this->createQueryBuilder('w')
//            ->andWhere('w.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
