<?php

namespace App\Repository;

use App\Entity\Device;
use App\Entity\Location;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Device>
 *
 * @method Device|null find($id, $lockMode = null, $lockVersion = null)
 * @method Device|null findOneBy(array $criteria, array $orderBy = null)
 * @method Device[]    findAll()
 * @method Device[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeviceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Device::class);
    }

    /** @return ?\App\Entity\Location[] */
    public function findAllByLocation(Location $location): array
    {
        return $this->findBy(['location' => $location]);
    }

    public function getAllDevicesByLocation(Location $location)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('d.name AS device, e.entityId, e.name, do.domainClass, do.color, do.name AS domainName, do.unit, do.field')
            ->from(Location::class, 'l')
            ->leftJoin('l.devices', 'd')
            ->leftJoin('d.entities', 'e')
            ->leftJoin('e.domains', 'do')
            ->where('l = :location')
            ->setParameter('location', $location)
        ;

        return $qb->getQuery()->getResult();
    }

    /** @param Device[] */
    public function removeLocationFromDevice(array $entities): void
    {
        foreach ($entities as $entity){
            $entity->setLocation(null);
        }
    }

//    /**
//     * @return Device[] Returns an array of Device objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('d.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Device
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
