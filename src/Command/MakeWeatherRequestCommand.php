<?php

namespace App\Command;

use App\APIWeather\APIWeatherProcessor;
use App\APIWeather\APIWeatherProvider;
use App\APIWeather\APIWeatherProviderInterface;
use App\APIWeather\OpenWeatherMapProvider;
use App\Repository\LocationRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:make-weather-request',
    description: 'API Weather request',
)]
class MakeWeatherRequestCommand extends Command
{


    protected APIWeatherProviderInterface $weatherProvider;

    protected LocationRepository $locationRepostiry;

    public function __construct(OpenWeatherMapProvider $weatherProvider, LocationRepository $locationRepostiry)
    {
        parent::__construct();
        $this->weatherProvider = $weatherProvider;
        $this->locationRepostiry = $locationRepostiry;
    }

    protected function configure(): void
    {
       
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $latlngLocations = $this->locationRepostiry->getLocationsWithCoordinates();
        $weatherProcessor = new APIWeatherProcessor();
        $weatherProcessor->setAPIWeatherProvider($this->weatherProvider);

        foreach ($latlngLocations as $location) {
            try {
                $weatherProcessor->processRequest($location);

                $output->writeln(sprintf("<bg=green>Location %s success</>", $location->getName()));
            } catch (\Exception $ex){
                $output->writeln(sprintf("<error>Error: %s</error>", $ex->getMessage()));
            }
        }

        return Command::SUCCESS;
    }
}
