<?php

namespace App\APIWeather;
use App\APIWeather\APIWeatherProvider;
use App\APIWeather\APIWeatherProviderInterface;
use App\Entity\Location;
use App\Entity\Weather;
use App\Entity\WeatherData\OpenWeatherDataAPIResponse;
use App\Response\WeatherResponse;
use App\Serializer\WeatherDataSerializer;
use App\Service\APIService;
use App\Service\Timestamp;
use App\Service\WeatherService;
use Symfony\Component\Serializer\Serializer;

class OpenWeatherMapProvider implements APIWeatherProviderInterface {

    protected $apiService;
    protected string $apiKey;
    private WeatherService $weatherService;

    const PROVIDER = 'app.api_weather_provider.open_weather_map_provider';

    public function __construct(APIService $apiService, WeatherService $weatherService, string $apiKey)
    {
        $this->apiService = $apiService;
        $this->apiKey = $apiKey;
        $this->weatherService = $weatherService;
    }
    
    public function processRequest(Location $location): void
    {
        $url = $this->geturl();
        $response = $this->apiService->get($url, [
            'lat' => $location->getLat(),
            'lon' => $location->getLng(),
            'appid' => $this->apiKey,
            'units' => 'metric',
            'lang'=>'sk'
        ]);

        $this->weatherService->saveWeatherData($response, $location, self::PROVIDER);
    }

    public function processResponse(Weather $weather): WeatherResponse
    {
        $response = $weather->getResponse();
        $serializer = new WeatherDataSerializer();

        $responseObject = $serializer->deserializeFromJson($response, OpenWeatherDataAPIResponse::class);

        $weatherResponse = new WeatherResponse();

        $sunrise = $responseObject->getSys()['sunrise'];
        $sunset = $responseObject->getSys()['sunset'];

        $sunriseHours = Timestamp::getTime($sunrise);
        $sunsetHours = Timestamp::getTime($sunset);
        $weatherInfo = $responseObject->getWeather()[0];
        $mainInfo = $responseObject->getMain();

        $icon = $this->getImageUrl($weatherInfo['icon']);
        
        $weatherResponse
            ->setSunrise($sunriseHours)
            ->setSunset($sunsetHours)
            ->setIcon($icon)
            ->setWeatherDesc($weatherInfo['description'])
            ->setHumidity(sprintf("%s %%", $mainInfo['humidity']))
            ->setTemperature(sprintf("%s °C", $mainInfo['temp']))
        ;

        return $weatherResponse;
    }

    private function geturl() {
        return "https://api.openweathermap.org/data/2.5/weather";
    }

    private function getImageUrl(string $code) {
        return sprintf("https://openweathermap.org/img/wn/%s@2x.png", $code);
    }

}