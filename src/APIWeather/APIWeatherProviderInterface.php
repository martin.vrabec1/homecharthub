<?php

namespace App\APIWeather;
use App\Entity\Location;
use App\Entity\Weather;
use App\Response\WeatherResponse;

interface APIWeatherProviderInterface {

    public function processRequest(Location $location);

    public function processResponse(Weather $weather): WeatherResponse;

}