<?php

namespace App\APIWeather;
use App\Entity\Location;
use App\Entity\Weather;

class APIWeatherProcessor {
    
    private ?APIWeatherProviderInterface $apiWeatherRequest;

    public function __construct()
    {
        $this->apiWeatherRequest = null;
    }

    public function setAPIWeatherProvider(APIWeatherProviderInterface $apiWeatherRequest): void
    {
        $this->apiWeatherRequest = $apiWeatherRequest;
    }

    public function processRequest(Location $location)
    {
        if ($this->apiWeatherRequest){
            $this->apiWeatherRequest->processRequest($location);
        } else {
            throw new \Exception("APIWeatherRequest strategy is not set");
        }
    }

    public function processResponse(Weather $weather)
    {
        if ($this->apiWeatherRequest){
           return $this->apiWeatherRequest->processResponse($weather);
        } else {
            throw new \Exception("APIWeatherRequest strategy is not set");
        }
    }


}