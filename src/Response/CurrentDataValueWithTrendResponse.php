<?php

namespace App\Response;
use App\Enums\Trend;

class CurrentDataValueWithTrendResponse implements ArrayResponse {
    
    private string $device; 

    private string $time;

    private string $value;

    private string $domain;

    private ?string $trend;

    private float|string $rawValue;

    private string $unit;

    private string $name;

    public function getTrend(): ?string
    {
        return $this->trend;
    }

    public function setTrend (?string $value): ?self
    {
        $this->trend = $value;
        return $this;
    }

    public function getDevice(): ?string
    {
        return $this->device;
    }

    public function setDevice (?string $device): self
    {
        $this->device = $device;
        return $this;
    }

    public function getTime(): string
    {
        return $this->time;
    }

    public function setTime (string $time): self
    {
        $this->time = $time;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue (string $value): self
    {
        $this->value = $value;
        return $this;
    }

    public function getRawValue(): float
    {
        return $this->rawValue;
    }

    public function setRawValue (float|string $value): self
    {
        $this->rawValue = $value;
        return $this;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function setDomain (string $value): self
    {
        $this->domain = $value;
        return $this;
    }

    public function getUnit(): string
    {
        return $this->unit;
    }

    public function setUnit (string $value): self
    {
        $this->unit = $value;
        return $this;
    }

    public function getName(): string
    {
        return $this->domain;
    }

    public function setName (string $text): self
    {
        $this->name = $text;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'device' => $this->device,
            'time' => $this->time,
            'value' => $this->value,
            'domain' => $this->domain,
            'trend' => $this->trend,
            'rawValue' => $this->rawValue,
            'unit' => $this->unit,
            'name' => $this->name
        ];
    }

}