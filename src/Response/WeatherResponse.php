<?php

namespace App\Response;

class WeatherResponse implements ArrayResponse {

    private string $sunrise;
    private string $sunset;
    private string $icon;
    private string $temperature;
    private string $humidity;
    private string $weatherDesc;

    public function getSunrise()
    {
        return $this->sunrise;
    }

    public function setSunrise(string $sunrise)
    {
        $this->sunrise = $sunrise;
        return $this;
    }

    public function getSunset()
    {
        return $this->sunrise;
    }

    public function setSunset(string $sunset)
    {
        $this->sunset = $sunset;
        return $this;
    }
    public function getIcon()
    {
        return $this->sunrise;
    }

    public function setIcon(string $icon)
    {
        $this->icon = $icon;
        return $this;
    }

    public function getTemperature()
    {
        return $this->temperature;
    }

    public function setTemperature(string $temperature)
    {
        $this->temperature = $temperature;
        return $this;
    }

    public function getHumidity()
    {
        return $this->temperature;
    }

    public function setHumidity(string $humidity)
    {
        $this->humidity = $humidity;
        return $this;
    }

    public function getWeatherDesc()
    {
        return $this->temperature;
    }

    public function setWeatherDesc(string $weatherDesc)
    {
        $this->weatherDesc = $weatherDesc;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'sunrise' => $this->sunrise,
            'sunset' => $this->sunset,
            'icon' => $this->icon,
            'temperature' => $this->temperature,
            'humidity' => $this->humidity,
            'weatherDesc' => $this->weatherDesc
        ];
    }

}