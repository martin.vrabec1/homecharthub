<?php

namespace App\Response;
use App\Service\Timestamp;

class DateTimeValueResponse implements StringResponse {

    private string $datetime;

    private string $value;

    public function getDatetime(): ?string
    {
        return $this->datetime;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setDatetime(string $datetime): self
    {
        $this->datetime = $datetime;
        return $this;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;
        return $this;
    }

    public function __toString()
    {
        return sprintf("%s %s", $this->value, $this->datetime);
    }
}