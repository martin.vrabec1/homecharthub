<?php

namespace App\Response;

interface ArrayResponse {
    public function toArray(): array;
}