<?php

namespace App\Response;

class DataForChatResponse {

    private $chatItemResponses = [];

    public function addChatItemResponse(DataForChatItemResponse $chatItem)
    {
        $this->chatItemResponses[] = $chatItem;
    }

    public function getChatItems(): array
    {
        return $this->chatItemResponses;
    }

    public function toArray():array {
        return array_map(function($item){
            return $item->toArray();
        }, $this->chatItemResponses);
    }

}