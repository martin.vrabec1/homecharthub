<?php

namespace App\Response;
use App\Service\DataService;


class DewpointSensorCurrentValueDecorator extends CurrentDataValueWithTrendResponse {

    private SensorCurrentValueWithTrendResponse $sensorCurrentValueWithTrendResponse;

    public function __construct(SensorCurrentValueWithTrendResponse $sensorCurrentValueWithTrendResponse)
    {
        $this->sensorCurrentValueWithTrendResponse = $sensorCurrentValueWithTrendResponse;
    }

    public function toArray(): array
    {
        $tempAndHumid = $this->getTemperatureAndHumidity();

        $actual = $this->sensorCurrentValueWithTrendResponse->toArray();

        $firstResponse = $actual[0];

        if (isset($tempAndHumid['temperature']) && isset($tempAndHumid['humidity'])) {
            $dewpoint = $this->getDewPoint(
                $tempAndHumid['temperature'],
                $tempAndHumid['humidity']
            );

            $dewpointResponse = [...$firstResponse, 
                'value'=> $dewpoint . ' °C', 
                'domain' => 'dewpoint',
                'trend' => null,
                'rawValue' => $dewpoint,
                'unit' => ' °C',
                'name' => 'Rosný bod',
            ];

            $actual[] = $dewpointResponse;

        }

        return $actual;
    }

    private function getTemperatureAndHumidity()
    {
        $temperature = null;
        $humidity = null;

        $array = $this->sensorCurrentValueWithTrendResponse->toArray();
        
        $temperature = array_filter($array, function($item){
            return $item['domain'] == 'temperature';
        });
        $temperature = reset($temperature)['rawValue'];

        $humidity = array_filter($array, function($item){
            return $item['domain'] == 'humidity';
        });
        $humidity = reset($humidity)['rawValue'];

        return [
            'temperature' => $temperature,
            'humidity' => $humidity,
        ];
    }

    private function getDewPoint($temperature, $humidity)
    {
        return round(DataService::calculateDewPoint($temperature, $humidity),2);
    }

}
