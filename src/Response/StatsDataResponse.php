<?php

namespace App\Response;

class StatsDataResponse implements ArrayResponse {

    private string $count;

    private string $firstValueTime;

    private DateTimeValueResponse $max;

    private DateTimeValueResponse $min;

    public function getCount(): ?string
    {
        return $this->count;
    }

    public function setCount(?string $count): self
    {
        $this->count = $count;
        return $this;
    }

    public function getFirstValueTime(): ?string
    {
        return $this->firstValueTime;
    }

    public function setFirstValueTime(?string $valueTime): self
    {
        $this->firstValueTime = $valueTime;
        return $this;
    }

    public function getMax(): ?DateTimeValueResponse
    {
        return $this->max;
    }

    public function setMax(?DateTimeValueResponse $max): self
    {
        $this->max = $max;
        return $this;
    }

    public function getMin(): ?DateTimeValueResponse
    {
        return $this->min;
    }

    public function setMin(?DateTimeValueResponse $min): self
    {
        $this->min = $min;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'total' => $this->count,
            'first' => $this->firstValueTime,
            'min' => (string)$this->min,
            'max' => (string)$this->max,
        ];
    }

    

}