<?php

namespace App\Response;

class DataForChatItemResponse implements ArrayResponse
{

    private $data;
    private $color;
    private $label;
    private $yAxisId;

    public function setData($data): self
    {
        $this->data = $data;
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setColor($color): self
    {
        $this->color = $color;
        return $this;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setLabel($label): self
    {
        $this->label = $label;
        return $this;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getYAxisId() 
    {
        return $this->yAxisId;
    }

    public function setYAxisId(string $axisId): self
    {
        $this->yAxisId = $axisId;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'data' => $this->data,
            'color' => $this->color,
            'label' => $this->label,
            'yAxisID' => $this->yAxisId
        ];
    }

}