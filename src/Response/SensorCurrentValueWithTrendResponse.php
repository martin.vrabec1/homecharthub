<?php

namespace App\Response;

class SensorCurrentValueWithTrendResponse implements ArrayResponse {

    private $entityResponses = [];

    public function addEntityResponse(CurrentDataValueWithTrendResponse $response)
    {
        $this->entityResponses[] = $response;

        return $this;
    }

    public function getEntityResponses()
    {
        return $this->entityResponses;
    }

    public function toArray():array
    {
        return array_map(function($item){
            return $item->toArray();
        },$this->entityResponses);
    }

}