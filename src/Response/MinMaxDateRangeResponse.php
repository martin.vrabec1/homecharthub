<?php 

namespace App\Response;

class MinMaxDateRangeResponse implements ArrayResponse{

    private $domainResponses = [];

    public function addDomainResponse(MinMaxDateRangeDomainResponse $response):self
    {
        $this->domainResponses[] = $response;
        return $this;
    }

    public function getDomainResponses()
    {
        return $this->domainResponses;
    }
    
    public function toArray():array
    {
        return array_map(function($item){
            return $item->toArray();
        }, $this->domainResponses);
    }



}

?>