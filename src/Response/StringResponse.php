<?php

namespace App\Response;

interface StringResponse {
    public function __toString(): string;
}