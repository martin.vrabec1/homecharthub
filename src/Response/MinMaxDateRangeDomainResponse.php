<?php

namespace App\Response;

class MinMaxDateRangeDomainResponse implements ArrayResponse {
    private string $domainClass;

    private string $domainName;

    private string $min;

    private string $max;

    public function setDomainClass($domainClass):self
    {
        $this->domainClass = $domainClass;

        return $this;
    }

    public function getDomainClass():?string
    {
        return $this->domainClass;
    }

    public function setDomainName($domainName):self
    {
        $this->domainName = $domainName;

        return $this;
    }

    public function getDomainName():?string
    {
        return $this->domainName;
    }

    public function setMin($min):self
    {
        $this->min = $min;

        return $this;
    }

    public function getMin():?string
    {
        return $this->min;
    }

    public function setMax($max):self
    {
        $this->max = $max;

        return $this;
    }

    public function getMax():?string
    {
        return $this->max;
    }

    public function toArray(): array
    {
        return [
            'domain' => $this->domainClass,
            'domainName' => $this->domainName,
            'min' => $this->min,
            'max' => $this->max
        ];
    }
    
}