<?php

namespace App\Entity\WeatherData;

class OpenWeatherDataAPIResponse
{
    private $coord;
    private $weather;
    private $base;
    private $main;
    private $visibility;
    private $wind;
    private $clouds;
    private $dt;
    private $sys;
    private $timezone;
    private $cityId;
    private $name;
    private $cod;

    public function getCoord(): array
    {
        return $this->coord;
    }

    public function setCoord(array $coord): void
    {
        $this->coord = $coord;
    }

    public function getWeather(): array
    {
        return $this->weather;
    }

    public function setWeather(array $weather): void
    {
        $this->weather = $weather;
    }

    public function getBase(): string
    {
        return $this->base;
    }

    public function setBase(string $base): void
    {
        $this->base = $base;
    }

    public function getMain(): array
    {
        return $this->main;
    }

    public function setMain(array $main): void
    {
        $this->main = $main;
    }

    public function getVisibility(): int
    {
        return $this->visibility;
    }

    public function setVisibility(int $visibility): void
    {
        $this->visibility = $visibility;
    }

    public function getWind(): array
    {
        return $this->wind;
    }

    public function setWind(array $wind): void
    {
        $this->wind = $wind;
    }

    public function getClouds(): array
    {
        return $this->clouds;
    }

    public function setClouds(array $clouds): void
    {
        $this->clouds = $clouds;
    }

    public function getDt(): int
    {
        return $this->dt;
    }

    public function setDt(int $dt): void
    {
        $this->dt = $dt;
    }

    public function getSys(): array
    {
        return $this->sys;
    }

    public function setSys(array $sys): void
    {
        $this->sys = $sys;
    }

    public function getTimezone(): int
    {
        return $this->timezone;
    }

    public function setTimezone(int $timezone): void
    {
        $this->timezone = $timezone;
    }

    public function getCityId(): int
    {
        return $this->cityId;
    }

    public function setCityId(int $cityId): void
    {
        $this->cityId = $cityId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getCod(): int
    {
        return $this->cod;
    }

    public function setCod(int $cod): void
    {
        $this->cod = $cod;
    }
}