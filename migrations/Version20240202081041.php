<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240202081041 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE domain ADD color VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE weather RENAME INDEX fk_4cd0d36e64d218e TO IDX_4CD0D36E64D218E');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE weather RENAME INDEX idx_4cd0d36e64d218e TO FK_4CD0D36E64D218E');
        $this->addSql('ALTER TABLE domain DROP color');
    }
}
